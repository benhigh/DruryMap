package xyz.benhigh.drurymap

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONException
import android.content.Intent
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val MARKER_COLOR_BUILDING = BitmapDescriptorFactory.HUE_RED
private const val MARKER_COLOR_PARKING = BitmapDescriptorFactory.HUE_BLUE
private const val MARKER_COLOR_DORM = BitmapDescriptorFactory.HUE_GREEN
private const val MARKER_COLOR_DEFAULT = BitmapDescriptorFactory.HUE_YELLOW

private const val MARKER_TYPE_BUILDING = 1
private const val MARKER_TYPE_PARKING = 2
private const val MARKER_TYPE_DORM = 3
private const val MARKER_TYPE_OTHER = 4

@Parcelize
class MapMarker(var label: String, var location: LatLng, var hue: Float, var description: String?,
                var type: Int, var imagePath: String) : Parcelable {
	lateinit var markerObj: Marker
}

class MainActivity : AppCompatActivity(), OnMapReadyCallback {
	private lateinit var map: GoogleMap
	private val markers = ArrayList<MapMarker>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		// Open the locations file
		val json = assets.open("locations.json").bufferedReader().use {
			it.readText()
		}

		// Parse the locations from the file
		try {
			val jsonArr = JSONArray(json)
			(0 until jsonArr.length())
					.map { jsonArr.getJSONObject(it) }
					.mapTo(markers) {
						MapMarker(it.getString("name"),
								LatLng(it.getDouble("lat"), it.getDouble("lng")),
								when {
									it.getString("type") == "building" -> MARKER_COLOR_BUILDING
									it.getString("type") == "parking" -> MARKER_COLOR_PARKING
									it.getString("type") == "dorm" -> MARKER_COLOR_DORM
									else -> MARKER_COLOR_DEFAULT
								},
								when {
									it.getString("type") == "building" -> it.getString("description")
									it.getString("type") == "parking" -> it.getString("description")
									else -> null
								},
								when {
									it.getString("type") == "building" -> MARKER_TYPE_BUILDING
									it.getString("type") == "parking" -> MARKER_TYPE_PARKING
									it.getString("type") == "dorm" -> MARKER_TYPE_DORM
									else -> MARKER_TYPE_OTHER
								},
								it.getString("image"))
					}
		}
		catch (e: JSONException) {
			val alert = AlertDialog.Builder(this).create()
			alert.setTitle(getString(R.string.dialog_json_err_title))
			alert.setMessage(getString(R.string.dialog_json_err_message, e.message))
			alert.setCancelable(false)
			alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", { _, _ ->
				finish()
			})
			alert.show()
		}

		// Add the locations to the text list
		for (marker in markers) {
			val textView = Button(this)
			textView.text = marker.label
			textView.textSize = 20f
			textView.setOnClickListener {
				map.moveCamera(CameraUpdateFactory.newLatLng(marker.location))
			}
			scrollLayout.addView(textView)
		}

		mapView3.onCreate(savedInstanceState)
		mapView3.getMapAsync(this)
	}

	override fun onMapReady(map: GoogleMap) {
		this.map = map

		// Set the boundaries of the map
		val boundSW = LatLng(37.215917, -93.290118)
		val boundNE = LatLng(37.222976, -93.281871)
		map.setLatLngBoundsForCameraTarget(LatLngBounds(boundSW, boundNE))

		// Zoom in and make the map hybrid (show satellite and road names)
		map.setMinZoomPreference(15.5f)
		map.mapType = GoogleMap.MAP_TYPE_HYBRID

		// Enable location, if possible
		try {
			map.isMyLocationEnabled = true
		} catch (e: SecurityException) {
			Toast.makeText(this, getString(R.string.location_unavailable), Toast.LENGTH_LONG).show()
		}

		// Add the markers onto the map
		for (marker in markers) {
			val markerSettings = MarkerOptions()
					.position(marker.location)
					.title(marker.label)
					.icon(BitmapDescriptorFactory.defaultMarker(marker.hue))
			if (marker.description != null)
				markerSettings.snippet(marker.description)
			val markerObj = map.addMarker(markerSettings)
			markerObj.tag = marker
			marker.markerObj = markerObj
		}

		// Adjust UI settings
		map.uiSettings.isTiltGesturesEnabled = false
		map.uiSettings.isRotateGesturesEnabled = false
		map.uiSettings.isZoomControlsEnabled = true

		map.setOnInfoWindowClickListener { marker ->
			val intent = Intent(this, InfoActivity::class.java)
			val mapMarker = marker.tag as MapMarker
			intent.putExtra("location", mapMarker)
			startActivity(intent)
		}

		// Move the camera to Drury
		val drury = LatLng(37.219649, -93.286156)
		map.moveCamera(CameraUpdateFactory.newLatLng(drury))
	}

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onPause() {
		mapView3.onPause()
		super.onPause()
	}

	override fun onResume() {
		mapView3.onResume()
		super.onResume()
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when {
			// Show a credits popup
			item.itemId == R.id.creditButton -> {
				val alert = AlertDialog.Builder(this).create()
				alert.setTitle(getString(R.string.dialog_title))
				alert.setMessage(getString(R.string.dialog_message))
				alert.setCancelable(false)
				alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", { dialog, _ ->
					dialog.dismiss()
				})
				alert.show()
			}
			// Toggle whether markers of a certain type are visible
			else -> {
				val subject = when {
					item.itemId == R.id.buildingsMenuButton -> MARKER_TYPE_BUILDING
					item.itemId == R.id.parkingMenuButton -> MARKER_TYPE_PARKING
					item.itemId == R.id.dormsMenuButton -> MARKER_TYPE_DORM
					else -> MARKER_TYPE_OTHER
				}
				item.isChecked = !item.isChecked
				markers
						.filter { it.type == subject }
						.forEach { it.markerObj.isVisible = item.isChecked }
			}
		}
		return true
	}
}
