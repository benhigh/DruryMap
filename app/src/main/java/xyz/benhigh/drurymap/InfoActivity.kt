package xyz.benhigh.drurymap

import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_info)

		// Load in our location from the calling intent
		val location = intent.getParcelableExtra<MapMarker>("location")

		// Set the text objects
		title = location.label
		buildingDescription.text = location.description
		buildingImage.contentDescription = getString(R.string.info_image_text, location.label)

		// Create a drawable out of the specified asset, and set the image to it
		val inputStream = assets.open(location.imagePath)
		val drawable = Drawable.createFromStream(inputStream, null)
		buildingImage.setImageDrawable(drawable)
		inputStream.close()
	}
}
